Extends [airbnb](https://github.com/airbnb/javascript)

Tries not to deviate from airbnb too much. Available rule sets:

- `wolf`
- `wolf/react`
- `wolf/tests`
- `wolf/react-tests`

## Installing

```
npm install --save-dev gitlab:keksas/eslint-config-wolf
```

Create `.eslintrc` file
```
{
    "extends": "wolf"
}
```

Add script to `package.json` to use
```
"lint": "eslint src/"
```

## With tests

```
touch tests/.eslintrc
echo '{"extends": "wolf/tests"}' > tests/.eslintrc
```

## Editor config

It is recommended to use following `.editorconfig` file
```
root = true
[*]
end_of_line = lf
insert_final_newline = true
charset = utf-8
indent_style = space
indent_size = 2
```
