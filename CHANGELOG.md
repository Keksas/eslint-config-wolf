# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [2.3.0] 2019-05-28
### Added
- React rules now use babel-eslint parser to properly support es6

### Changed
- Updated eslint and it's plugins dependencies

## [2.2.1] 2017-06-11
### Added
- proxyquire global

### Changed
- turned off import/no-extraneous-dependencies

## [2.2.0] 2017-06-11
### Added
- Rules for react tests

## [2.1.0] 2017-05-21
### Added
- React rule set

## [2.0.0] 2017-04-30
### Added
- Unit test rules

### Changed
- Shareable eslint rules are now shared according to [eslint](http://eslint.org/docs/developer-guide/shareable-configs) recommendation.

